defmodule IdopterForum.Repo do
  use Ecto.Repo,
    otp_app: :idopter_forum,
    adapter: Ecto.Adapters.Postgres

  use Kerosene, per_page: 5
end
