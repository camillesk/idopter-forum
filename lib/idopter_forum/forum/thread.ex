defmodule IdopterForum.Forum.Thread do
  use Ecto.Schema
  import Ecto.Changeset

  alias IdopterForum.Forum.Comment

  schema "threads" do
    field :author, :string
    field :description, :string
    field :title, :string

    timestamps()

    has_many :comments, Comment
  end

  @doc false
  def changeset(thread, attrs) do
    thread
    |> cast(attrs, [:title, :description, :author])
    |> validate_required([:title, :description, :author])
  end
end
