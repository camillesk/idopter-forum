defmodule IdopterForum.Forum.Comment do
  use Ecto.Schema
  import Ecto.Changeset

  alias IdopterForum.Forum.Thread

  schema "comments" do
    field :author, :string
    field :text, :string

    timestamps()

    belongs_to :thread, Thread
  end

  @doc false
  def changeset(comment, attrs) do
    comment
    |> cast(attrs, [:text, :author, :thread_id])
    |> validate_required([:text, :author, :thread_id])
  end
end
