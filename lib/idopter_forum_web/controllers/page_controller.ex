defmodule IdopterForumWeb.PageController do
  use IdopterForumWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
