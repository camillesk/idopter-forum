defmodule IdopterForumWeb.ThreadController do
  use IdopterForumWeb, :controller

  alias IdopterForum.Forum
  alias IdopterForum.Forum.{Comment, Thread}
  alias IdopterForum.Repo

  def index(conn, params) do
    {threads, kerosene} =
      Forum.list_threads()
      |> Repo.paginate(params)

    render(conn, "index.html", threads: threads, kerosene: kerosene)
  end

  def new(conn, _params) do
    changeset = Forum.change_thread(%Thread{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"thread" => thread_params}) do
    case Forum.create_thread(thread_params) do
      {:ok, thread} ->
        conn
        |> put_flash(:info, "Thread created successfully.")
        |> redirect(to: Routes.thread_path(conn, :show, thread))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    thread =
      Forum.get_thread!(id)
      |> Repo.preload([:comments])

    changeset = Comment.changeset(%Comment{}, %{})
    render(conn, "show.html", thread: thread, changeset: changeset)
  end

  def edit(conn, %{"id" => id}) do
    thread = Forum.get_thread!(id)
    changeset = Forum.change_thread(thread)
    render(conn, "edit.html", thread: thread, changeset: changeset)
  end

  def update(conn, %{"id" => id, "thread" => thread_params}) do
    thread = Forum.get_thread!(id)

    case Forum.update_thread(thread, thread_params) do
      {:ok, thread} ->
        conn
        |> put_flash(:info, "Thread updated successfully.")
        |> redirect(to: Routes.thread_path(conn, :show, thread))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", thread: thread, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    thread = Forum.get_thread!(id)
    {:ok, _thread} = Forum.delete_thread(thread)

    conn
    |> put_flash(:info, "Thread deleted successfully.")
    |> redirect(to: Routes.thread_path(conn, :index))
  end

  def add_comment(conn, %{"comment" => comment_params, "thread_id" => thread_id}) do
    thread = thread_id

    Forum.get_thread!(thread_id)
    |> Repo.preload([:comments])

    case Forum.add_comment(thread_id, comment_params) do
      {:ok, _comment} ->
        conn
        |> put_flash(:info, "Comment created successfully.")
        |> redirect(to: Routes.thread_path(conn, :show, thread))

      {:error, _error} ->
        conn
        |> put_flash(:error, "Error while creating comment")
        |> redirect(to: Routes.thread_path(conn, :show, thread))
    end
  end
end
