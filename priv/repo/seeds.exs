# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     IdopterForum.Repo.insert!(%IdopterForum.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias IdopterForum.Forum.{Comment, Thread}

thread1 =
  IdopterForum.Repo.insert!(%Thread{
    title: "Title 1",
    description: "Description 1",
    author: "Seeds"
  })

thread2 =
  IdopterForum.Repo.insert!(%Thread{
    title: "Title 2",
    description: "Description 2",
    author: "Seeds"
  })

IdopterForum.Repo.insert!(%Thread{title: "Title 3", description: "Description 3", author: "Seeds"})

IdopterForum.Repo.insert!(%Comment{text: "nice comment 1", author: "Seeds", thread: thread1})
IdopterForum.Repo.insert!(%Comment{text: "nice comment 2", author: "Seeds", thread: thread1})
IdopterForum.Repo.insert!(%Comment{text: "nice comment 3", author: "Seeds", thread: thread2})
