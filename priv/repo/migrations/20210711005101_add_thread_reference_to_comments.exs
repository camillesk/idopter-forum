defmodule IdopterForum.Repo.Migrations.AddThreadReferenceToComments do
  use Ecto.Migration

  def change do
    alter table(:comments) do
      add :thread_id, references(:threads)
    end
  end
end
