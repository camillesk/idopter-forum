defmodule IdopterForum.Repo.Migrations.ChangeTextFieldType do
  use Ecto.Migration

  def change do
    alter table(:comments) do
      modify :text, :text
    end
  end
end
