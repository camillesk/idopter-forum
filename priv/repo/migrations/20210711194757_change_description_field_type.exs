defmodule IdopterForum.Repo.Migrations.ChangeDescriptionFieldType do
  use Ecto.Migration

  def change do
    alter table(:threads) do
      modify :description, :text
    end
  end
end
