defmodule IdopterForum.Repo.Migrations.CreateThreads do
  use Ecto.Migration

  def change do
    create table(:threads) do
      add :title, :string
      add :description, :string
      add :author, :string

      timestamps()
    end

  end
end
