# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :idopter_forum,
  ecto_repos: [IdopterForum.Repo]

# Configures the endpoint
config :idopter_forum, IdopterForumWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "A8bh60WAYJRGiacauFfjLq2iWH341jE3UmC5jIas8cm/9Jt7Q386wo2cAc/ZlSyK",
  render_errors: [view: IdopterForumWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: IdopterForum.PubSub,
  live_view: [signing_salt: "Q2nlCo3/"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :kerosene,
  theme: :materialize

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
